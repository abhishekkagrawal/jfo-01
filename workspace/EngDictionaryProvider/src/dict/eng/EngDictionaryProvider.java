package dict.eng;

import java.util.HashMap;
import java.util.Map;

import dict.Dictionary;

public class EngDictionaryProvider implements Dictionary {
	private String name;
	private Map<String, String> map;
	public EngDictionaryProvider() {
		name = "english";
		map = new HashMap<String, String>();
		map.put("apple", "a fruit");
		map.put("rest", "cease work or movement");
		map.put("grow", "increase");
	}
	@Override
	public String getDefinition(String word) {
		if(map.containsKey(word))
			return name + "->" + map.get(word);
		return null;
	}
}
