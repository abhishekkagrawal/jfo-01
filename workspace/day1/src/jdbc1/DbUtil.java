package jdbc1;

import java.sql.Driver;
import java.sql.DriverManager;

public class DbUtil {
	public static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	public static final String DB_URL = "jdbc:derby:D:\\jfde-01\\private\\workspace\\db";
	public static final String DB_USER = "";
	public static final String DB_PASSWORD = "";

//	public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
//	public static final String DB_URL = "jdbc:mysql://localhost:3306/test";
//	public static final String DB_USER = "user";
//	public static final String DB_PASSWORD = "pass";
	
	static {
		try {
			Class<?> cls = Class.forName(DB_DRIVER);
			Driver drv = (Driver) cls.newInstance();
			DriverManager.deregisterDriver(drv);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
