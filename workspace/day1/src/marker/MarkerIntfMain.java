package marker;

// marker interface
// if class is inherited from this interface, it is allowed to convert to string.
interface Stringify {	
}

// verify if object's class is inherited from marker
// and if inherited then only convert it to string
class StringifyUtil {
	public static String stringify(Object obj) {
		if(obj instanceof Stringify)
			return obj.toString();
		throw new RuntimeException("This object is cannot be strinify.");
	}
}

class MyClass implements Stringify {
	private int data1;
	private String data2;
	public MyClass(int data1, String data2) {
		this.data1 = data1;
		this.data2 = data2;
	}
	@Override
	public String toString() {
		return "MyClass [data1=" + data1 + ", data2=" + data2 + "]";
	}
}


class YourClass {
	private int data1;
	private String data2;
	public YourClass(int data1, String data2) {
		this.data1 = data1;
		this.data2 = data2;
	}
	@Override
	public String toString() {
		return "YourClass [data1=" + data1 + ", data2=" + data2 + "]";
	}
}

public class MarkerIntfMain {
	public static void main(String[] args) {
		String res1 = StringifyUtil.stringify(new MyClass(101, "Dummy"));
		System.out.println(res1);
		
		String res2 = StringifyUtil.stringify(new YourClass(102, "dummy"));
		System.out.println(res2);
	}
}


