package proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class AccountProxyHandler implements InvocationHandler {
	private IAccount target;
	public AccountProxyHandler(IAccount target) {
		this.target = target;
	}


	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// pre-processing
		System.out.println("Before calling: " + method.getName());
		if(method.getName().equals("deposit") || method.getName().equals("withdraw")) {
			Double amount = (Double) args[0];
			if(amount!=null && amount < 0)
				throw new RuntimeException("-ve amount cannot be deposit or withdraw");
		}
		
		// calling business logic
		Object result = method.invoke(target, args);
		
		// post-processing
		System.out.println("After calling: " + method.getName());
		
		return result;
	}
	
	public static IAccount getProxy(IAccount original) {
		AccountProxyHandler handler = new AccountProxyHandler(original);
		Object proxy = Proxy.newProxyInstance(
				IAccount.class.getClassLoader(), 
				new Class<?>[] {IAccount.class}, 
				handler);
	
		/*
		Class<?> proxyCls = Proxy.getProxyClass(IAccount.class.getClassLoader(), 
				 new Class<?>[] {IAccount.class});
		System.out.println("Proxy class: " + proxyCls.getName());
		Object proxy = proxyCls.newInstance();
		// add handler of proxy
		*/
		return (IAccount) proxy;
	}
}





