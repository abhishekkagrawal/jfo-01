package annotations;

import java.io.Serializable;
import java.lang.annotation.Annotation;

import javax.annotation.Resource;

@Resource(description="this is for testing")
@Readme(value="A class", info="Demo class", author="Nilesh Ghule")
class A {
}

@Readme(value="B class") // info & author are default
class B {
	
}

@Readme("C class") // value is implicit member
class C {
	
}

public class AnnotationMain {
	public static void main(String[] args) {
		Annotation[] anns = A.class.getDeclaredAnnotations();
		for (Annotation ann : anns) {
			System.out.println(ann);
		}
		System.out.println();	
		
		anns = B.class.getDeclaredAnnotations();
		for (Annotation ann : anns) {
			if(ann instanceof Readme) {
				Readme r = (Readme) ann;
				System.out.println(r.value() + " --> " + r.author());
			}
		}
		
		Readme r = C.class.getDeclaredAnnotation(Readme.class);
		if(r!=null)
			System.out.println(r.value() + " --> " + r.author());
	}
}

