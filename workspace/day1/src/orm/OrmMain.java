package orm;

import java.util.List;

public class OrmMain {
	public static void main(String[] args) {
		try(OrmSession session = new OrmSession()) {
			List<Object> list = session.findAll(Dept.class);
			for (Object d : list)
				System.out.println(d);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();
		
		try(OrmSession session = new OrmSession()) {
			Object d = session.get(Dept.class, 20);
			System.out.println(d);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
