package di;

public class DIMain {
	public static void main(String[] args) {
		Person p0 = new Person("Nilesh", "Pune", "37");
		System.out.println(p0.toString());
		
		AppContext ctx = new AppContext();
		Person p1 = (Person) ctx.getBean("p1");
		System.out.println(p1);
		Person p2 = (Person) ctx.getBean("p2");
		System.out.println(p2);
		Person p3 = (Person) ctx.getBean("p3");
		System.out.println(p3);
	}
}
