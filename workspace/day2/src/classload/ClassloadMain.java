package classload;

import java.lang.reflect.Method;

public class ClassloadMain {
	public static void main(String[] args) throws Exception {
		System.out.println(ClassloadMain.class.getClassLoader());
		System.out.println(Class.forName("sun.net.spi.nameservice.dns.DNSNameService").getClassLoader());
		System.out.println(Object.class.getClassLoader());	
		System.out.println(ClassloadMain.class.getClassLoader().getParent());
		System.out.println(ClassloadMain.class.getClassLoader().getParent().getParent());
	
//		try {
//			CustomClassLoader ccl = new CustomClassLoader();
//			Class<?> c = ccl.loadClass("hello.HelloWorld");
//			System.out.println("Class loaded: " + c.getName());
//			Method main = c.getMethod("main", new String[0].getClass());
//			Object[] cmdlineArgs = { new String[0] }; 
//			main.invoke(null, cmdlineArgs);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}
}
