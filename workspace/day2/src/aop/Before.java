// ***Java Framework Developer Essentials***
// Simple AOP framework - POC implementation
// Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
// Date: 6-Jun-2020

package aop;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(METHOD)
public @interface Before {
	String value();
}
