// ***Java Framework Developer Essentials***
// Simple AOP framework - POC implementation
// Author: Nilesh Ghule <nilesh@sunbeaminfo.com>
// Date: 6-Jun-2020

package aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class AopHandler implements InvocationHandler {
	private Object target;
	private Class<?> aspectCls;
	private Object aspect;

	public AopHandler(Object target, Class<?> aspectCls) {
		this.target = target;
		this.aspectCls = aspectCls;
		try {
			this.aspect = aspectCls.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Method findBeforeAdvice(String methodName) {
		Method[] adviceMethods = aspectCls.getDeclaredMethods();
		for (Method advice : adviceMethods) {
			Before ann = advice.getDeclaredAnnotation(Before.class);
			if(ann != null && ann.value().equals(methodName))
				return advice;
		}
		return null;
	}
	
	public Method findAfterAdvice(String methodName) {
		Method[] adviceMethods = aspectCls.getDeclaredMethods();
		for (Method advice : adviceMethods) {
			After ann = advice.getDeclaredAnnotation(After.class);
			if(ann != null && ann.value().equals(methodName))
				return advice;
		}
		return null;
	}
	
	public Method findAfterThrowingAdvice(String methodName) {
		Method[] adviceMethods = aspectCls.getDeclaredMethods();
		for (Method advice : adviceMethods) {
			AfterThrowing ann = advice.getDeclaredAnnotation(AfterThrowing.class);
			if(ann != null && ann.value().equals(methodName))
				return advice;
		}
		return null;
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object result = null;
		try {
			Method beforeAdvice = findBeforeAdvice(method.getName());
			if(beforeAdvice!=null)
				beforeAdvice.invoke(aspect, target, args);
			
			// invoke method on target object
			result = method.invoke(target, args);

			Method afterAdvice = findAfterAdvice(method.getName());
			if(afterAdvice!=null)
				afterAdvice.invoke(aspect, target, args);
		}catch (Throwable e) {
//			e.printStackTrace();
			
			Method afterThrowingAdvice = findAfterThrowingAdvice(method.getName());
			if(afterThrowingAdvice!=null)
				afterThrowingAdvice.invoke(aspect, e.getCause(), target, args);
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getProxy(Object original, Class<T> intfCls, Class<?> aspectCls) {
		try {
			return (T) Proxy.newProxyInstance(
						AopHandler.class.getClassLoader(), 
						new Class<?>[] { intfCls },
						new AopHandler(original, aspectCls));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
