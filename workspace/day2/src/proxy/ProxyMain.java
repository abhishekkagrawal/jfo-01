package proxy;

public class ProxyMain {
	public static void main(String[] args) {
//		IAccount acc = new AccountImpl(1, 5000);
		IAccount acc = AccountProxyHandler.getProxy( new AccountImpl(1, 5000) );
		System.out.println("Object created: " + acc.getClass().getName());
		acc.withdraw(3000);
		System.out.println("Updated balance: " + acc.getBalance());
		acc.deposit(1500);
		System.out.println("Updated balance: " + acc.getBalance());
		
		acc.deposit(-100);
	}
}
