package dict;

import java.util.ServiceLoader;

public class DictionaryService {
	private static DictionaryService provider = new DictionaryService();
	public static DictionaryService getInstance() {
		return provider;
	}
	
	private ServiceLoader<Dictionary> loader;
	public DictionaryService() {
		loader = ServiceLoader.load(Dictionary.class);
	}
	
	public String getDefinition(String word) {
		StringBuilder result = new StringBuilder();
		for (Dictionary dictProvider : loader) {
			String defn = dictProvider.getDefinition(word);
			if(defn != null) {
				result.append(dictProvider.getDefinition(word));
				result.append(",");
			}
		}
		if(result.length() == 0)
			result.append("not available");
		return word + " : " + result.toString();
	}

}
