package transactions;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class TxManagementHandler implements InvocationHandler {
	private Object target;
	
	public TxManagementHandler(Object target) {
		this.target = target;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Method targetMethod = target.getClass().getDeclaredMethod(method.getName(), method.getParameterTypes());
		Transactional transactional = targetMethod.getDeclaredAnnotation(Transactional.class);
//		System.out.println("On method " + method + " transactional = " + transactional);
		Object result = null;
		try {
			if(transactional!=null) {
				GenericDao.getCurrentDao().getCon().setAutoCommit(false);
				System.out.println("Before: " + method.getName() + " begin transaction.");
			}
			
			result = method.invoke(target, args);
			System.out.println(method.getName() + " result: " + result);
			
			if(transactional!=null) {
				GenericDao.getCurrentDao().getCon().commit();
				System.out.println("After: " + method.getName() + " commit transaction.");
			}
		} catch (Throwable e) {
//			e.printStackTrace();
			if(transactional!=null) {
				GenericDao.getCurrentDao().getCon().rollback();
				System.out.println("After Throwing: " + method.getName() + " rollback transaction.");
			}
			throw e.getCause();
		} finally {
			System.out.print("TxManagementHandler finally - ");
			GenericDao.getCurrentDao().close();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public static<T> T getProxy(Class<T> serviceClass) {
		try {
			Object target = serviceClass.newInstance();
			return (T) Proxy.newProxyInstance(TxManagementHandler.class.getClassLoader(), serviceClass.getInterfaces(),
					new TxManagementHandler(target));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
