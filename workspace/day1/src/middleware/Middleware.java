package middleware;

import java.lang.reflect.Method;

public class Middleware {
	public static Object invoke(String className, String methodName, Class<?>[] argTypes, Object[] argValues) {
		try {
			// load the class & get its metadata (java.lang.Class)
			Class<?> c = Class.forName(className);
			// create object of the class at runtime
			Object obj = c.newInstance(); // calls param-less ctor
			// find the method with given name and arg types
			Method m = c.getDeclaredMethod(methodName, argTypes);
			// allow accessing private methods
			m.setAccessible(true);
			// invoke the method on given object and with given args and collect result
			Object res = m.invoke(obj, argValues);
			// return result
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}
