package dict;

public interface Dictionary {
	String getDefinition(String word);
}
