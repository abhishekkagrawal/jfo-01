package reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Scanner;

public class ReflectionMain {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter class name: ");
		String className = sc.nextLine();
		try {
			Class<?> c = Class.forName(className);
			
			System.out.println("Name: " + c.getName());
			
			Class<?> scls = c.getSuperclass();
			if(scls != null)
				System.out.println("SuperClass: " + scls.getName());
			
			Class<?>[] icls = c.getInterfaces();
			for (Class<?> ic : icls)
				System.out.println("Super Interface: " + ic.getName());
		
			Field[] fields = c.getDeclaredFields();
			for (Field field : fields)
				System.out.println("Field : " + field.toString());
			
			Constructor<?>[] ctors = c.getDeclaredConstructors();
			for (Constructor<?> ctor : ctors)
				System.out.println("Ctor : " + ctor.toString());
			
			Method[] methods = c.getDeclaredMethods();
			for (Method method : methods)
				System.out.println("Method : " + method.toString());
			
			Annotation[] anns = c.getDeclaredAnnotations();
			for (Annotation ann : anns)
				System.out.println("Annotation : " + ann.toString());
		
			System.out.println("Is Interface: " + c.isInterface());
			System.out.println("Is Member class: " + c.isMemberClass());
		} catch (Exception e) {
			e.printStackTrace();
		}
		sc.close();
	}
}
