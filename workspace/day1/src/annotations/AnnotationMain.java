package annotations;

import java.io.Serializable;
import java.lang.annotation.Annotation;

import javax.annotation.Resource;

@Resource(description="just for testing")
@Readme(value="A class", info="This is simple class", author="Nilesh Ghule")
class A {
	
}

@Readme(value="B class") // info & author are optional -- follow default values
class B {
	
}

@Readme("C class") // value is implicit member
class C {
	
}

public class AnnotationMain {
	public static void main(String[] args) {
		Annotation[] anns = A.class.getDeclaredAnnotations();
		for (Annotation ann : anns) {
			System.out.println(ann);
		}
		System.out.println();
		
		Annotation[] anns2 = B.class.getDeclaredAnnotations();
		for (Annotation ann : anns2) {
			if(ann instanceof Readme) {
				Readme r = (Readme) ann;
				System.out.println("value = " + r.value());
				System.out.println("author = " + r.author());
			}
		}
		System.out.println();
		
		Readme ann = C.class.getDeclaredAnnotation(Readme.class);
		System.out.println("value = " + ann.value());
		System.out.println("author = " + ann.author());
	}
}

