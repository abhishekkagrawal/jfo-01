package orm;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jdbc1.DbUtil;

public class OrmSession implements AutoCloseable {
	private Connection con;
	public OrmSession() throws Exception {
		con = DriverManager.getConnection(DbUtil.DB_URL);
	}
	@Override
	public void close() {
		try {
			if (con != null)
				con.close();
		} catch (Exception e) {
		}
	}
	public List<Object> findAll(Class<?> c) {
		List<Object> list = new ArrayList<Object>();
		Table table = c.getDeclaredAnnotation(Table.class);
		if(table == null)
			throw new RuntimeException("@Table must be present on entity class.");
		String tableName = table.value();
		Field[] fields = c.getDeclaredFields();
		Map<String,String> cols = new HashMap<>();
		for (Field field : fields) {
			String columnName = field.getName();
			Column column = field.getAnnotation(Column.class);
			if(column != null)
				columnName = column.value();
			cols.put(columnName, field.getName());
		}
		
		StringBuilder sql = new StringBuilder("SELECT ");
		for (String colName : cols.keySet())
			sql.append(colName + ",");
		sql.deleteCharAt(sql.length()-1);
		sql.append(" FROM " + tableName);
		
 		try(PreparedStatement stmt = con.prepareStatement(sql.toString())) {
   			ResultSet rs = stmt.executeQuery();
  			while(rs.next()) {
  				Object rec = c.newInstance();
  	 			for (String colName : cols.keySet()) {
  					String fieldName = cols.get(colName);
					Object fieldValue = rs.getObject(colName);
					Field f = c.getDeclaredField(fieldName);
					f.setAccessible(true);
					f.set(rec, fieldValue);
				}
  				list.add(rec);
  			}
 		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public Object get(Class<?> c, Object id) {
		Object result = null;
		Table table = c.getDeclaredAnnotation(Table.class);
		if(table == null)
			throw new RuntimeException("@Table must be present on entity class.");
		String tableName = table.value();
		String idColName = "";
		Field[] fields = c.getDeclaredFields();
		Map<String,String> cols = new HashMap<>();
		for (Field field : fields) {
			String columnName = field.getName();
			Column column = field.getAnnotation(Column.class);
			if(column != null)
				columnName = column.value();
			cols.put(columnName, field.getName());
			Id idAnn = field.getAnnotation(Id.class);
			if(idAnn != null)
				idColName = column.value();
		}
		
		StringBuilder sql = new StringBuilder("SELECT ");
		for (String colName : cols.keySet())
			sql.append(colName + ",");
		sql.deleteCharAt(sql.length()-1);
		sql.append(" FROM " + tableName + " WHERE " + idColName + " = ?");
		
 		try(PreparedStatement stmt = con.prepareStatement(sql.toString())) {
 			stmt.setObject(1, id);
   			ResultSet rs = stmt.executeQuery();
  			while(rs.next()) {
  				result = c.newInstance();
  	 			for (String colName : cols.keySet()) {
  					String fieldName = cols.get(colName);
					Object fieldValue = rs.getObject(colName);
					Field f = c.getDeclaredField(fieldName);
					f.setAccessible(true);
					f.set(result, fieldValue);
				}
  			}
 		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
