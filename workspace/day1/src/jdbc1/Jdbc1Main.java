package jdbc1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Jdbc1Main {
	public static void main(String[] args) {
		String sql = "SELECT deptno, dname, loc FROM dept";
		try(Connection con = DriverManager.getConnection(DbUtil.DB_URL, DbUtil.DB_USER, DbUtil.DB_PASSWORD)) {
			try(PreparedStatement stmt = con.prepareStatement(sql)) {
				try(ResultSet rs = stmt.executeQuery()) {
					while(rs.next()) {
						int deptno = rs.getInt("deptno");
						String dname = rs.getString("dname");
						String loc = rs.getString("loc");
						System.out.println(deptno + ", " + dname + ", " + loc);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
