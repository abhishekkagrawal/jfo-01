package aop;

public class AccountAspects {
	@Before("deposit")
	public void beforeDeposit(Object target, Object[] args) {
		System.out.println("Before depositing Rs. " + args[0]);
	}
	@After("deposit")
	public void afterDeposit(Object target, Object[] args) {
		System.out.println("After depositing Rs. " + args[0]);
	}
	
	@Before("withdraw")
	public void beforeWithdarw(Object target, Object[] args) {
		System.out.println("Before withdrawing Rs. " + args[0]);
	}
	@After("withdraw")
	public void afterWithraw(Object target, Object[] args) {
		System.out.println("After withdrawing Rs. " + args[0]);
	}
	@AfterThrowing("withdraw")
	public void afterWithrawFailed(Throwable ex, Object target, Object[] args) {
		System.out.println("Failed while withdrawing Rs. " + args[0]);
		System.out.println("Exception: " + ex.getMessage());
	}
}
