package dict.tech;

import java.util.HashMap;
import java.util.Map;

import dict.Dictionary;

public class TechDictionaryProvider implements Dictionary {
	private String name;
	private Map<String, String> map;
	public TechDictionaryProvider() {
		name = "technical";
		map = new HashMap<String, String>();
		map.put("apple", "a computer/mobile company");
		map.put("rest", "representational state transfer");
		map.put("c", "c programming language");
	}
	@Override
	public String getDefinition(String word) {
		if(map.containsKey(word))
			return name + "->" + map.get(word);
		return null;
	}
}
