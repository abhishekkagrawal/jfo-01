package di;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import jdbc1.DbUtil;

public class AppContext {
	private Map<String, Object> beans = new HashMap<>();
	private Map<String, String> di = new HashMap<>();
	private Map<String, String> types = new HashMap<>();
	private Map<String, Map<String, String>> fieldTypes = new HashMap<>();
	private Map<String, Map<String, String>> fieldValues = new HashMap<>();
	
	public AppContext() {
		loadBeanConfig();
		createBeans();
	}

	public void loadBeanConfig() {
		Properties props = new Properties();
		try(InputStream in = DbUtil.class.getResourceAsStream("/di/di.properties")) {
			props.load(in);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		Enumeration<String> itr = (Enumeration<String>) props.propertyNames();
		while(itr.hasMoreElements()) {
			String key = itr.nextElement();
			String value = props.getProperty(key);
			String objName = key.split("\\.")[0];
			
			if(key.contains("object")) {
				types.put(objName, value);
				fieldTypes.put(objName, new LinkedHashMap<>());
				fieldValues.put(objName, new LinkedHashMap<>());
			}
			else if(key.contains("di"))
				di.put(objName, value);
		}
		
		itr = (Enumeration<String>) props.propertyNames();
		while(itr.hasMoreElements()) {
			String key = itr.nextElement();
			String value = props.getProperty(key);
			String objName = key.split("\\.")[0];
			
			String fieldName = key.split("\\.")[1];
			if(key.contains("type")) {
				fieldTypes.get(objName).put(fieldName, value);
			}
			if(key.contains("value")) {
				fieldValues.get(objName).put(fieldName, value);
			}
		}
	}
	
	public void createBeans() {
		for (Entry<String, String> diEntry : di.entrySet()) {
			if(diEntry.getValue().equals("setter"))
				setterBasedDI(diEntry.getKey());
			else if(diEntry.getValue().equals("constructor"))
				constructorBasedDI(diEntry.getKey());
			else
				fieldBasedDI(diEntry.getKey());
		}
	}
	
	public Object setterBasedDI(String beanName) {
		Object bean = null;
		try {
			Class<?> c = Class.forName(types.get(beanName));
			bean = c.newInstance();
			Map<String, String> beanFieldTypes = fieldTypes.get(beanName);
			Map<String, String> beanFieldValues = fieldValues.get(beanName);
			for (String fieldName: beanFieldTypes.keySet()) {
				String fieldType = beanFieldTypes.get(fieldName);
				String fieldValue = beanFieldValues.get(fieldName);
				
				String methodName = "set" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
				//System.out.println(methodName);
				Method m = c.getDeclaredMethod(methodName, Class.forName(fieldType));
				m.invoke(bean, fieldValue);
			}
			beans.put(beanName, bean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}
	
	public Object constructorBasedDI(String beanName) {
		Object bean = null;
		try {
			Class<?> c = Class.forName(types.get(beanName));
			Map<String, String> beanFieldTypes = fieldTypes.get(beanName);
			Map<String, String> beanFieldValues = fieldValues.get(beanName);
			
			String[] ctorArgTypes = beanFieldTypes.values().toArray(new String[0]);
			Object[] ctorArgValues = beanFieldValues.values().toArray();

			Class<?>[] ctorArgClasses = new Class<?>[ctorArgTypes.length];
			for (int i = 0; i < ctorArgClasses.length; i++)
				ctorArgClasses[i] = Class.forName(ctorArgTypes[i]);
			Constructor<?> ctor = c.getDeclaredConstructor(ctorArgClasses);
			bean = ctor.newInstance(ctorArgValues);
			beans.put(beanName, bean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}
	
	public Object fieldBasedDI(String beanName) {
		Object bean = null;
		try {
			Class<?> c = Class.forName(types.get(beanName));
			bean = c.newInstance();
			Map<String, String> beanFieldValues = fieldValues.get(beanName);
			for (String fieldName: beanFieldValues.keySet()) {
				String fieldValue = beanFieldValues.get(fieldName);				
				Field f = c.getDeclaredField(fieldName);
				f.setAccessible(true);
				f.set(bean, fieldValue);
			}
			beans.put(beanName, bean);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bean;
	}
	
	public Object getBean(String beanName) {
		return beans.get(beanName);
	}
}
