package transactions;

import java.util.Arrays;
import java.util.List;

public class TransactionsMain {
	public static void main(String[] args) {
		DeptService service = TxManagementHandler.getProxy(DeptServiceImpl.class);
		Integer cnt;
		
		try {
			Dept[] newDepts = {new Dept(50, "Development", "Mumbai"), new Dept(10, "Training", "Pune")};
			cnt = service.addDepts(Arrays.asList(newDepts));
			System.out.println("Depts added: " + cnt + "\n");
		} catch (Exception e) {
			System.out.println("Exception while adding: " + e.getMessage());
		}
		System.out.println();
		
		try {
			List<Dept> result = service.allDepts();
			result.forEach(System.out::println);
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println();
		
		try {
			Integer[] deptIds = { 50, 60 };
			cnt = service.delDepts(Arrays.asList(deptIds));
			System.out.println("Depts deleted: " + cnt + "\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
