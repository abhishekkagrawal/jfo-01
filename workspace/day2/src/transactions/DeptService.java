package transactions;

import java.util.List;

public interface DeptService {

	Integer addDepts(List<Dept> list);

	Integer delDepts(List<Integer> idList);

	List<Dept> allDepts();

}