package classload;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CustomClassLoader extends ClassLoader {
	private static final String dirPath = "D:/temp";
	private byte[] loadClassFileData(String fileName) throws IOException {
		File clsFile = new File(dirPath, fileName);
		try(InputStream stream = new FileInputStream(clsFile)) {
			byte buff[] = new byte[(int) clsFile.length()];
			DataInputStream in = new DataInputStream(stream);
			in.readFully(buff);
			return buff;
		}
	}
	private Class<?> getClass(String name) throws ClassNotFoundException {
		String file = name.replace('.', File.separatorChar) + ".class";
		byte[] b = null;
		try {
			b = loadClassFileData(file);
			Class<?> c = defineClass(name, b, 0, b.length);
			resolveClass(c);
			return c;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	@Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        System.out.println("Loading Class '" + name + "'");
        Class<?> cls = null;        
        try {
			cls = super.loadClass(name);
		} catch (ClassNotFoundException e) {
			cls = getClass(name);
			if(cls == null)
				throw new ClassNotFoundException("Class " + name + " not found under " + dirPath);
		}
        return cls;
    }
}
