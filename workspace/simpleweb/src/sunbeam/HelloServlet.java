package sunbeam;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.println("Servlet served : " + request.getContextPath() + "<br/>");
		out.println("Servlet loaded at : " + new Date() + "<br/>");
		ClassLoader clsloader = this.getClass().getClassLoader();
		out.println("Servlet classloader : " + clsloader.getClass().getName() + "<br/>");
		out.println("Servlet find location : " + Arrays.toString(((URLClassLoader)clsloader).getURLs()) + "<br/>");
		ClassLoader parentclsloader = clsloader.getParent();
		out.println("Classloader parent : " + parentclsloader.getClass().getName() + "<br/>");
		out.println("Classloader grand-parent : " + parentclsloader.getParent() + "<br/>");
		out.println("Classloader grand-grand-parent : " + parentclsloader.getParent().getParent() + "<br/>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

// ********************** OUTPUT (on my machine) *************************
//Servlet served : /simpleweb
//Servlet loaded at : Sun Jun 07 08:43:16 IST 2020
//Servlet classloader : org.apache.catalina.loader.ParallelWebappClassLoader
//Servlet find location : [file:/D:/jfde-01/workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/simpleweb/WEB-INF/classes/]
//Classloader parent : java.net.URLClassLoader
//Classloader grand-parent : sun.misc.Launcher$AppClassLoader@33909752
//Classloader grand-grand-parent : sun.misc.Launcher$ExtClassLoader@7cf10a6f

