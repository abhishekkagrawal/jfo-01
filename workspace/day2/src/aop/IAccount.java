package aop;

public interface IAccount {
	void setId(int id);
	int getId();
	void setBalance(double balance);
	double getBalance();
	void deposit(double amount);
	void withdraw(double amount);
}
