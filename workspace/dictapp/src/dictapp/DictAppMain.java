package dictapp;

import dict.DictionaryService;

public class DictAppMain {
	public static void main(String[] args) {
		DictionaryService dict = DictionaryService.getInstance();
		String result = dict.getDefinition("rest");
		System.out.println(result);		
	}
}
