package config;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
//import org.yaml.snakeyaml.Yaml;


class DbConfig {
	private String name;
	private String url;
	private String driver;
	private String user;
	private String password;
	public DbConfig() {
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "DbConfig [name=" + name + ", url=" + url + ", driver=" + driver + ", user=" + user + ", password="
				+ password + "]";
	}
}

class DbConfigs {
	private ArrayList<DbConfig> dbs = new ArrayList<>();
	public ArrayList<DbConfig> getDbs() {
		return dbs;
	}
	public void setDbs(ArrayList<DbConfig> dbs) {
		this.dbs = dbs;
	}
}

public class ConfigMain {
	public static void main(String[] args) {
//		loadPropertiesConfigFile("/config/jdbc.properties");
//		loadXmlConfigFile("/config/jdbc.xml");
		loadYamlConfigFile("/config/jdbc.yml");
	}

	@SuppressWarnings("unchecked")
	private static void loadYamlConfigFile(String configFile) {
//		Yaml yaml = new Yaml();
//		try(InputStream in = ConfigMain.class.getResourceAsStream(configFile)) {
//			Map<String,ArrayList<Object>> dbs = yaml.load(in);
//			ArrayList<Object> list = dbs.get("dbs");
//			for (Object obj : list) {
//				Map<String, String> dbcfg = (Map<String, String>) obj;
//				dbcfg.forEach((key,value)->System.out.println(key + " => " + value));
//				System.out.println();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}

	private static void loadXmlConfigFile(String configFile) {
		Properties props = new Properties();
		try(InputStream in = ConfigMain.class.getResourceAsStream(configFile)) {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(in);
			Element root = doc.getDocumentElement();
			System.out.println(root.getTagName());
			NodeList childNodes = root.getChildNodes();
			for (int i = 0; i < childNodes.getLength(); i++) {
				Node node = childNodes.item(i);
				if(node.getNodeType() == Node.ELEMENT_NODE) {
					Element childEle = (Element) node;
					System.out.println(" - " + childEle.getTagName());
					
					NodeList grandChildNodes = childEle.getChildNodes();
					for (int j = 0; j < grandChildNodes.getLength(); j++) {
						Node temp = grandChildNodes.item(j);
						if(temp.getNodeType() == Node.ELEMENT_NODE) {
							Element grandChildEle = (Element) temp;
							System.out.print("   - " + grandChildEle.getTagName());
							System.out.println(" => " + grandChildEle.getTextContent());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void loadPropertiesConfigFile(String configFile) {
		Properties props = new Properties();
		try(InputStream in = ConfigMain.class.getResourceAsStream(configFile)) {
			props.load(in);
			for (String key : props.stringPropertyNames()) {
				String value = props.getProperty(key);
				System.out.println(key + " => " + value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
