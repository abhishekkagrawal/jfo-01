package aop;

public class AccountImpl implements IAccount {
	private int id;
	private double balance;
	public AccountImpl() {
		this(0, 0.0);
	}
	public AccountImpl(int id, double balance) {
		this.id = id;
		this.balance = balance;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public void deposit(double amount) {
		this.balance += amount;
	}
	public void withdraw(double amount) {
		if(this.balance - amount < 0.0)
			throw new RuntimeException("insufficient balance.");
		this.balance -= amount;
	}
	@Override
	public String toString() {
		return "AccountImpl [id=" + id + ", balance=" + balance + "]";
	}
}
