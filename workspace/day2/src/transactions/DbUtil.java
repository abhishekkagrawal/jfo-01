package transactions;

import java.io.InputStream;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Properties;

public class DbUtil {
	public static final Properties props = new Properties();
	public static final String DB_URL;
	public static final String DB_DRIVER;
	public static final String DB_USER;
	public static final String DB_PASSWORD;
	
	static {
		try(InputStream in = DbUtil.class.getResourceAsStream("/transactions/jdbc.properties")) {
			props.load(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		DB_URL = props.getProperty("db.url", "");
		DB_DRIVER = props.getProperty("db.driver", "");
		DB_USER = props.getProperty("db.user", "");
		DB_PASSWORD = props.getProperty("db.password", "");
		
		try {
			Class<?> c = Class.forName(DB_DRIVER);
			Driver drv = (Driver) c.newInstance();
			DriverManager.registerDriver(drv);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}	
}
