package transactions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class GenericDao implements AutoCloseable {
	private static GenericDao curDao = null;
	public static GenericDao getCurrentDao() {
		if(curDao == null)
			return new GenericDao();
		return curDao;
	}
	
	
	private Connection con;
	public GenericDao() {
		try {
			con = DriverManager.getConnection(DbUtil.DB_URL, DbUtil.DB_USER, DbUtil.DB_PASSWORD);
			curDao = this;
			System.out.println("Connection created!");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void close() {
		try {
			curDao = null;
			if (con != null)
				con.close();
			con = null;
			System.out.println("Connection closed!");
		} catch (Exception e) {
		}
	}
	
	public Connection getCon() {
		return con;
	}
	
	public List<Object[]> executeQuery(String sql, Object... params) {
		List<Object[]> list = new ArrayList<Object[]>();
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			for (int i = 0; i < params.length; i++)
				stmt.setObject(i+1, params[i]);
			ResultSet rs = stmt.executeQuery();
			int colCount = rs.getMetaData().getColumnCount();
			while(rs.next()) {
				Object[] row = new Object[colCount];
				for (int i = 0; i < row.length; i++)
					row[i] = rs.getObject(i+1);
				list.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return list;
	}
	
	public int executeUpdate(String sql, Object... params) {
		int cnt = -1;
		try(PreparedStatement stmt = con.prepareStatement(sql)) {
			for (int i = 0; i < params.length; i++)
				stmt.setObject(i+1, params[i]);
			cnt = stmt.executeUpdate();
		} catch (Exception e) {
			//e.printStackTrace();
			throw new RuntimeException(e);
		}
		return cnt;
	}
}
