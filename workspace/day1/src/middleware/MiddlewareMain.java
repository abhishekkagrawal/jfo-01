package middleware;

public class MiddlewareMain {
	public static void main(String[] args) {
		String result = (String) Middleware.invoke("java.util.Date", "toString", new Class<?>[] {}, new Object[] {});
		System.out.println("result = " + result);
		
		Integer addRes = (Integer) Middleware.invoke("middleware.Arithmetic", 
				"add", 
				new Class<?>[] {Integer.class, Integer.class},
				new Object[] {12, 6});
		System.out.println("add result: " + addRes);
		
		int multiplyRes = (Integer) Middleware.invoke("middleware.Arithmetic", 
				"multiply", 
				new Class<?>[] {Integer.TYPE, Integer.TYPE},
				new Object[] {12, 6});
		System.out.println("multiply result: " + multiplyRes);
	}
}
