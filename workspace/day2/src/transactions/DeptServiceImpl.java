package transactions;

import java.util.ArrayList;
import java.util.List;

public class DeptServiceImpl implements DeptService {
	@Transactional
	public Integer addDepts(List<Dept> list) {
		int cnt = 0;
		String sql = "INSERT INTO dept(deptno,dname,loc) VALUES(?,?,?)";
		GenericDao dao = GenericDao.getCurrentDao();
		for (Dept d : list) {
			cnt += dao.executeUpdate(sql, d.getId(), d.getDname(), d.getLoc());
		}
		return cnt;
	}
	
	@Transactional
	public Integer delDepts(List<Integer> idList) {
		int cnt = 0;
		String sql = "DELETE FROM dept WHERE deptno=?";
		GenericDao dao = GenericDao.getCurrentDao();
		for (Integer id : idList) {
			cnt += dao.executeUpdate(sql, id);
		}
		return cnt;
	}
	
	public List<Dept> allDepts() {
		List<Dept> list = new ArrayList<>();
		String sql = "SELECT deptno, dname, loc FROM dept";
		GenericDao dao = GenericDao.getCurrentDao();
		List<Object[]> rows = dao.executeQuery(sql);
		for (Object[] row : rows)
			list.add(new Dept((Integer)row[0], (String)row[1], (String)row[2]));
		return list;
	}
}


