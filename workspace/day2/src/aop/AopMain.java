package aop;

public class AopMain {
	public static void main(String[] args) {
		IAccount acc = AopHandler.getProxy(
							new AccountImpl(1, 5000),
							IAccount.class,
							AccountAspects.class);
		System.out.println("Proxy created: " + acc.getClass().getName());
		acc.withdraw(3000);
		System.out.println("Updated balance: " + acc.getBalance());
		acc.deposit(1500);
		System.out.println("Updated balance: " + acc.getBalance());
		acc.withdraw(4000);
		System.out.println("Updated balance: " + acc.getBalance());
	}
}
